<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 10/10/14
 * Time: 7:03 AM
 */

namespace Smorken\Report;


class Model {

    /**
     * @var Builder
     */
    protected $builder;
    /**
     * @var ReportManager
     */
    protected static $manager;

    public static function setManager(ReportManager $manager)
    {
        static::$manager = $manager;
    }

    public function getManager()
    {
        return static::$manager;
    }

    public function getBuilder()
    {
        return $this->builder;
    }

    public function extBuilder($builder)
    {
        $this->initializeBuilder($builder);
        return $this;
    }

    public function initializeBuilder($extBuilder)
    {
        $this->builder = static::$manager->getFactory()->makeBuilder($extBuilder);
    }

    public function __call($k, $v)
    {
        return call_user_func_array(array($this->builder, $k), $v);
    }
} 