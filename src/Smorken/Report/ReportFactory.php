<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 10/10/14
 * Time: 7:03 AM
 */

namespace Smorken\Report;


class ReportFactory {

    public function makeBuilder($externalBuilder)
    {
        $extGrammar = $this->getExternalGrammar($externalBuilder);
        $eb = $this->getExternalBuilder($externalBuilder);
        $grammar = $this->getGrammar($extGrammar);
        $builder = new Builder($grammar, $eb);

        return $builder;
    }

    public function getExternalBuilder($extModel)
    {
        $m = null;
        if (method_exists($extModel, 'newQuery')) {
            $m = $extModel->newQuery();
        }
        else if (method_exists($extModel, 'getQuery')) {
            $m = $extModel->getQuery();
        }
        else {
            throw new ReportException("No builder found.");
        }
        return $m;
    }

    public function getExternalGrammar($extModel)
    {
        $test = null;
        if (method_exists($extModel, 'getGrammar')) {
            $test = $extModel->getGrammar();
        }
        else if (method_exists($extModel, 'newQuery')) {
            $test = $this->getExternalGrammar($extModel->newQuery());
        }
        else if (method_exists($extModel, 'getQuery')) {
            $test = $this->getExternalGrammar($extModel->getQuery());
        }
        else {
            throw new ReportException("No grammar found.");
        }
        return $test;
    }

    public function getGrammar($extGrammar)
    {
        $name = class_basename($extGrammar);
        return $this->getGrammarFromBaseClass($name, $extGrammar);
    }

    public function getGrammarFromBaseClass($classname, $extGrammar)
    {
        $fullname = '\\Smorken\\Report\\Grammar\\' . $classname;
        if (class_exists($fullname)) {
            return new $fullname($extGrammar);
        }
        return new Grammar($extGrammar);
    }
} 