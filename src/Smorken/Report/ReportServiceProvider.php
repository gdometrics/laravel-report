<?php namespace Smorken\Report;

use Illuminate\Support\ServiceProvider;

class ReportServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Bootstrap the application events.
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->package('smorken/report', 'smorken/report');
        Model::setManager($this->app['smorken.report']);
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->bindShared('smorken.report.factory', function($app) {
            return new ReportFactory($app);
        });
        $this->app->bindShared('smorken.report', function($app) {
            return new ReportManager($app, $app['smorken.report.factory']);
        });
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array('smorken.report.factory', 'smorken.report');
	}

}
