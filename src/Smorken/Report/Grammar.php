<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 10/10/14
 * Time: 7:04 AM
 */

namespace Smorken\Report;

use Illuminate\Database\Query\Expression;

class Grammar {

    protected $extGrammar;

    public function __construct($extGrammar)
    {
        $this->extGrammar = $extGrammar;
    }

    public function compileAggregateExt(Builder $query, Aggregate $aggregate)
    {
        $columns = array();
        $column = $this->wrap($aggregate->getColumn());
        $columnas = $this->wrap($aggregate->getColumnAlias());
        $label = $labelas = false;
        if ($aggregate->getLabelColumn()) {
            $label = $this->wrap($aggregate->getLabelColumn());
            $labelas = $this->wrap($aggregate->getLabelAlias());
        }
        $columns[] = new Expression($aggregate->getType() . '(' . $column . ') as ' . $columnas);
        if ($label) {
            $columns[] = new Expression($label . ' as ' . $labelas);
        }
        return $columns;
    }

    public function __call($k, $v)
    {
        return call_user_func_array(array($this->extGrammar, $k), $v);
    }
} 