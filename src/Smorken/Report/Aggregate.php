<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 10/10/14
 * Time: 7:16 AM
 */

namespace Smorken\Report;


class Aggregate {

    public $type;

    public $column = '*';

    public $labelColumn;

    /**
     * @param string $type in count, avg, sum, min, max
     * @param array $args 0: column, 1: labelColumn
     * @throws ReportException
     */
    public function __call($type, $args)
    {
        $types = array('count', 'avg', 'sum', 'min', 'max');
        if (!in_array($type, $types)) {
            throw new ReportException("$type is not a valid aggregate function.");
        }
        $this->type = $type;
        $this->column = (isset($args[0]) ? $args[0] : '*');
        $this->labelColumn = (isset($args[1]) ? $args[1] : null);
    }

    public function getType()
    {
        return $this->type;
    }

    public function getColumn()
    {
        return $this->column;
    }

    public function getColumnAlias()
    {
        $a = $this->getColumnFromName($this->getLabelColumn());
        return sprintf('a__%s__%s', $this->getType(), $a);
    }

    public function getLabelColumn()
    {
        return $this->labelColumn ? : ($this->column !== '*' ? $this->column : 'star');
    }

    public function getLabelAlias()
    {
        $a = $this->getColumnFromName($this->getLabelColumn());
        return sprintf('l__%s__%s', $this->getType(), $a);
    }

    protected function getColumnFromName($name)
    {
        $parts = explode('.', $name);
        return last($parts);
    }

}