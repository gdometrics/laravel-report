<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 10/13/14
 * Time: 8:27 AM
 */

namespace Smorken\Report;


class AbstractChartableModel extends Model {

    protected $toplevels = array();

    protected $series = array();

    protected $seriesIsArray = true;

    protected $type = 'line';

    protected $options = array();

    public function getType()
    {
        return $this->type;
    }

    public function getOptions()
    {
        return $this->options;
    }

    public function parse($results)
    {
        $parsed = array();
        foreach($results as $row) {
            if ($this->toplevels) {
                $parsed = $this->topLevel($parsed, $row, $this->toplevels);
            }
            else {
                $this->series($parsed, $row, $this->series);
            }
        }
        return $parsed;
    }

    public function topLevel($parsed, $row, $levels = array())
    {
        if (!$levels) {
            $this->series($parsed, $row, $this->series);
            return $parsed;
        }
        list($col, $type) = $this->shift($levels);
        $tlname = $this->getLabelName($col, $type);
        if (!isset($parsed[$row->$tlname])) {
            $parsed[$row->$tlname] = array();
        }
        $parsed[$row->$tlname] = $this->topLevel($parsed[$row->$tlname], $row, $levels);
        return $parsed;
    }

    protected function series(&$parsed, $row, $series)
    {
        foreach($series as $col => $type) {
            $sname = $this->getLabelName($col, $type);
            if (!isset($parsed[$row->$sname])) {
                $parsed[$row->$sname] = array();
            }
            $aname = $this->getAggregateName($col, $type);
            if ($this->seriesIsArray) {
                $parsed[$row->$sname][] = $row->$aname;
            }
            else {
                $parsed[$row->$sname]['value'] = $row->$aname;
            }
        }
    }

    protected function getLabelName($col, $type)
    {
        if ($type === null) {
            return $col;
        }
        return $this->getName('l', $col, $type);
    }

    protected function getAggregateName($col, $type)
    {
        return $this->getName('a', $col, $type);
    }

    protected function getName($init, $col, $type)
    {
        return join('__', array($init, $type, $col));
    }

    /**
     * @return array
     */
    public function getSeries()
    {
        return $this->series;
    }

    /**
     * @param array $series
     */
    public function setSeries($series)
    {
        $this->series = $series;
    }

    /**
     * @return array
     */
    public function getToplevels()
    {
        return $this->toplevels;
    }

    /**
     * @param array $toplevels
     */
    public function setToplevels($toplevels)
    {
        $this->toplevels = $toplevels;
    }

    protected function shift(&$array)
    {
        list($k) = array_keys($array);
        $r  = array($k, $array[$k]);
        unset($array[$k]);
        return $r;
    }

} 