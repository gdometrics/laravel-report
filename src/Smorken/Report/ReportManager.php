<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 10/10/14
 * Time: 7:03 AM
 */

namespace Smorken\Report;


class ReportManager {

    /**
     * @var \Illuminate\Foundation\Application
     */
    protected $app;
    /**
     * @var \Smorken\Report\ReportFactory
     */
    protected $factory;

    public function __construct($app, ReportFactory $factory)
    {
        $this->app = $app;
        $this->factory = $factory;
    }

    public function getFactory()
    {
        return $this->factory;
    }

}