<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 10/10/14
 * Time: 7:28 AM
 */

namespace Smorken\Report;


class Builder {

    /**
     * @var Grammar
     */
    protected $grammar;
    protected $extBuilder;

    public function __construct(Grammar $grammar, $extBuilder)
    {
        $this->grammar = $grammar;
        $this->extBuilder = $extBuilder;
    }

    public function getExtBuilder()
    {
        return $this->extBuilder;
    }

    public function toSql()
    {
        return $this->grammar->compileSelect($this);
    }

    public function aggregate($type, $column = '*', $labelcolumn = null)
    {
        $aggregate = new Aggregate();
        $aggregate->$type($column, $labelcolumn);
        call_user_func_array(array($this->extBuilder, 'addSelect'), $this->grammar->compileAggregateExt($this, $aggregate));
        return $this;
    }

    public function __call($k, $v)
    {
        $r = call_user_func_array(array($this->extBuilder, $k), $v);
        if ($r === $this->extBuilder) {
            return $this;
        }
        return $r;
    }

    public function __get($k)
    {
        return $this->extBuilder->$k;
    }
}