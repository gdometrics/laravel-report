<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 10/23/14
 * Time: 3:04 PM
 */

use Smorken\Report\AbstractChartableModel;
use Mockery as m;

class ChartableModelTest extends PHPUnit_Framework_TestCase {

    /**
     * @var \Smorken\Report\AbstractChartableModel
     */
    protected $sut;

    public function setUp()
    {
        $this->sut = new AbstractChartableModel();
    }

    public function tearDown()
    {
        m::close();
    }

    public function testTopLevel()
    {
        $row = new \stdClass();
        $row->l__sel__foo = 'bar';
        $p = $this->sut->topLevel(array(), $row, array('foo' => 'sel'));
        $this->assertArrayHasKey('bar', $p);
    }

    public function testTopLevelTwoLevels()
    {
        $row = new \stdClass();
        $row->l__sel__foo = 'bar';
        $row->l__sel__fiz = 'baz';
        $p = $this->sut->topLevel(array(), $row, array('foo' => 'sel', 'fiz' => 'sel'));
        $this->assertArrayHasKey('baz', $p['bar']);
    }

    public function testTopLevelThreeLevels()
    {
        $row = new \stdClass();
        $row->l__sel__foo = 'bar';
        $row->l__sel__fiz = 'baz';
        $row->l__sel__faz = 'biz';
        $p = $this->sut->topLevel(array(), $row, array('foo' => 'sel', 'fiz' => 'sel', 'faz' => 'sel'));
        $this->assertArrayHasKey('baz', $p['bar']);
        $this->assertArrayHasKey('biz', $p['bar']['baz']);
    }

    public function testTopLevelSingleWithSeries()
    {
        $row = new \stdClass();
        $row->l__sel__foo = 'bar';
        $row->l__avg__foo ='foo-label';
        $row->a__avg__foo = 2;
        $this->sut->setSeries(array('foo' => 'avg'));
        $p = $this->sut->topLevel(array(), $row, array('foo' => 'sel'));
        $this->assertEquals(2, $p['bar']['foo-label'][0]);
    }

    public function testTopLevelTwoLevelsWithSeries()
    {
        $row = new \stdClass();
        $row->l__sel__foo = 'bar';
        $row->l__sel__fiz = 'baz';
        $row->l__avg__foo ='foo-label';
        $row->a__avg__foo = 2;
        $this->sut->setSeries(array('foo' => 'avg'));
        $p = $this->sut->topLevel(array(), $row, array('foo' => 'sel', 'fiz' => 'sel'));
        $this->assertEquals(2, $p['bar']['baz']['foo-label'][0]);
    }

    public function testParseSingleLevelWithSeries()
    {
        $row[0] = new \stdClass();
        $row[0]->l__sel__foo = 'bar';
        $row[0]->l__avg__foo ='foo-label';
        $row[0]->a__avg__foo = 1;
        $row[1] = new \stdClass();
        $row[1]->l__sel__foo = 'bar-2';
        $row[1]->l__avg__foo ='foo-label';
        $row[1]->a__avg__foo = 2;
        $this->sut->setSeries(array('foo' => 'avg'));
        $this->sut->setToplevels(array('foo' => 'sel'));
        $p = $this->sut->parse($row);
        $this->assertEquals(1, $p['bar']['foo-label'][0]);
        $this->assertEquals(2, $p['bar-2']['foo-label'][0]);
    }

    public function testParseTwoLevelsWithSeries()
    {
        $row[0] = new \stdClass();
        $row[0]->l__sel__foo = 'bar';
        $row[0]->l__sel__fiz = 'baz';
        $row[0]->l__avg__foo ='foo-label';
        $row[0]->a__avg__foo = 1;
        $row[1] = new \stdClass();
        $row[1]->l__sel__foo = 'bar-2';
        $row[1]->l__sel__fiz = 'baz-2';
        $row[1]->l__avg__foo ='foo-label';
        $row[1]->a__avg__foo = 2;
        $this->sut->setSeries(array('foo' => 'avg'));
        $this->sut->setToplevels(array('foo' => 'sel', 'fiz' => 'sel'));
        $p = $this->sut->parse($row);
        $this->assertEquals(1, $p['bar']['baz']['foo-label'][0]);
        $this->assertEquals(2, $p['bar-2']['baz-2']['foo-label'][0]);
    }
} 