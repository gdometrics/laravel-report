<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 10/10/14
 * Time: 9:53 AM
 */

use Mockery as m;

class BuilderTest extends \ReportTestCase {

    /**
     * @var \Smorken\Report\Builder
     */
    protected $sut;

    public function setUp()
    {
        parent::setUp();
        $this->grammar = m::mock('\Smorken\Report\Grammar');
        $this->extbuilder = m::mock('\Illuminate\Database\Query\Builder');
        $this->sut = new \Smorken\Report\Builder($this->grammar, $this->extbuilder);
    }

    public function testToSql()
    {
        $this->grammar->shouldReceive('compileSelect')->andReturn('bar');
        $this->assertEquals('bar', $this->sut->toSql());
    }

    public function testAggregate()
    {
        $this->grammar->shouldReceive('compileAggregate')->andReturn(array('foo', 'bar'));
        $this->extbuilder->shouldReceive('addSelect')->with('foo', 'bar')->andReturn($this->extbuilder);

        $this->assertEquals($this->sut, $this->sut->aggregate('count'));
    }

    public function testCallExtBuilderMethod()
    {
        $this->extbuilder->shouldReceive('foo')->andReturn($this->extbuilder);
        $this->assertEquals($this->sut, $this->sut->foo());
    }

    public function testCallExtBuilderMethodWithArgs()
    {
        $this->extbuilder->shouldReceive('foo')->with(1)->andReturn($this->extbuilder);
        $this->assertEquals($this->sut, $this->sut->foo(1));
    }

    public function testGetExtBuilderProperty()
    {
        $this->extbuilder->foo = 'bar';
        $this->assertEquals('bar', $this->sut->foo);
    }

    public function testCallExtBuilderNotExtBuilderReturnsValue()
    {
        $this->extbuilder->shouldReceive('foo')->andReturn('bar');
        $this->assertEquals('bar', $this->sut->foo());
    }
} 