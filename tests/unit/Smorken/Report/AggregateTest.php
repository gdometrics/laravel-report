<?php
use Smorken\Report\Aggregate;

/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 10/10/14
 * Time: 11:00 AM
 */


class AggregateTest extends PHPUnit_Framework_TestCase {

    /**
     * @var Aggregate
     */
    protected $sut;

    public function setUp()
    {
        $this->sut = new Aggregate();
    }

    public function testSimple()
    {
        $this->sut->count();
        $this->assertEquals('count', $this->sut->type);
        $this->assertEquals('*', $this->sut->column);
        $this->assertEquals('l__count__star', $this->sut->getLabelAlias());
        $this->assertEquals('a__count__star', $this->sut->getColumnAlias());
    }

    public function testSimpleWithColumn()
    {
        $this->sut->count('foo');
        $this->assertEquals('count', $this->sut->type);
        $this->assertEquals('foo', $this->sut->column);
        $this->assertEquals('l__count__foo', $this->sut->getLabelAlias());
        $this->assertEquals('a__count__foo', $this->sut->getColumnAlias());
    }

    public function testSimpleWithColumnAndLabel()
    {
        $this->sut->count('foo', 'foolabel');
        $this->assertEquals('count', $this->sut->type);
        $this->assertEquals('foo', $this->sut->column);
        $this->assertEquals('l__count__foolabel', $this->sut->getLabelAlias());
        $this->assertEquals('a__count__foolabel', $this->sut->getColumnAlias());
    }

    public function testDottedWithColumn()
    {
        $this->sut->count('db.foo');
        $this->assertEquals('count', $this->sut->type);
        $this->assertEquals('db.foo', $this->sut->column);
        $this->assertEquals('l__count__foo', $this->sut->getLabelAlias());
        $this->assertEquals('a__count__foo', $this->sut->getColumnAlias());
    }

    public function testDottedWithColumnAndLabel()
    {
        $this->sut->count('db.foo', 'db2.foolabel');
        $this->assertEquals('count', $this->sut->type);
        $this->assertEquals('db.foo', $this->sut->column);
        $this->assertEquals('l__count__foolabel', $this->sut->getLabelAlias());
        $this->assertEquals('a__count__foolabel', $this->sut->getColumnAlias());
    }
} 