<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 10/10/14
 * Time: 9:19 AM
 */


class ReportServiceProviderTest extends \ReportTestCase {

    public function setUp()
    {
        parent::setUp();

    }

    public function testGetReportFactory()
    {
        $f = $this->app['smorken.report.factory'];
        $this->assertInstanceOf('Smorken\Report\ReportFactory', $f);
    }

    public function testGetReportManager()
    {
        $m = $this->app['smorken.report'];
        $this->assertInstanceOf('Smorken\Report\ReportManager', $m);
    }
} 